# Inserção no Site e Customização

## Inserção no Site
É fácil inserir a Central da Privacidade no seu site. Idealmente, a Central da Privacidade deve estar presente em todas as páginas.  

Para fazer a inserção no site é necessário executar 2 passos:

1. Registro do CNAME
2. Inserção do código javascript em todas as páginas que devem mostrar a Central da Privacidade ou interagir com o objeto _window.dogma_ via javascript

#### Registro do CNAME
A internet funciona com endereços IP. Na versão 4, os endereços IP são do tipo `192.168.0.1`.  
Para acessar qualquer site na internet é necessário saber o endereço IP do servidor que hospeda o site que você gostaria de acessar, mas seria muito difícil saber de cabeça os endereços IP de todos os sites que nós acessamos.  

Por este motivo, todos os sites da internet são obrigados a utilizar o serviço de _DNS_. O _DNS_ é um serviço que transforma nomes de sites em endereços IP. É só por causa do _DNS_ que nós podemos acessar o _google.com_ sem saber o IP dele.

Para que a integração entre a Central da Privacidade e o seu site funcione, é necessário fazer uma alteração no _DNS_ do seu site, inserindo um registro do tipo _CNAME_. 
Por exemplo, se o seu site for nebers.com.br, o registro de _CNAME_ deve ser **dogma**.neber.com.br -> **api.dogma.legal**

O [painel de controle](https://console.dogmal.legal) indica em detalhes como fazer a configuração.


![Configuração DNS](img/dnsconfig.png "Instruções para fazer alteração de DNS")

#### Inserção do código no site
Além das configurações de _DNS_ descritas acima, é necessário inserir o código javascript gerado pela Dogma para a integração entre a Central da Privacidade e o seu site. Para fazer isso basta acessar o [painel de controle](https://console.dogmal.legal), copiar o código para o seu site, e colar o código em todas as páginas do seu site que devem mostrar a Central da Privacidade ou interagir com o objeto _window.dogma_ via javascript.


![Inserção do código da Central](img/centralconfig.png "Código disponível no Painel para ser copiado")

## Customização

É possível customizar a Central da Privacidade para as suas necessidades.  
As principais formas de customização são:

1. Temas
2. Texto
3. Funcionalidades
4. Estilo

#### Temas
Existem 3 temas de cores disponíveis: _blue_, _light_ e _dark_.
Para escolher um dos temas modifique a propriedade _theme_ do objeto _options_ que se encontra no código gerado:

```javascript
var options = {
    // ...
    theme: 'blue',
    // ...
};
```

#### Texto da Central da Privacidade
Para alterar o texto do central use a propriedade _text_ do objeto _options_:
```javascript
var options = {
    // ...
    text: 'Seu texto aqui',
    // ...
};
```

#### Texto da Página Inicial
Para alterar o texto da página de abertura da central da privacidade use a propriedade _home.text_ do objeto _options_:
```javascript
var options = {
    // ...
    home: {
        text: 'Seu texto aqui'
    }
    // ...
};
```

#### Funcionalidades
A Central da Privacidade oferece 3 funcionalidades principais: gestão de cookies, consentimentos e pedidos de titulares.
A configuração padrão habilita as 3 opções. Caso seja necessário desabilitar alguma funcionalidade, basta usar a propriedade _actions_ do objeto _options_. Exemplo:

```javascript
var options = {
    // ...
    actions  : ['cookies', 'inquiries', 'consent'],
    // ...
};
```

Para habilitar apenas a gestão de cookies use:
```javascript
var options = {
    // ...
    actions  : ['cookies'],
    // ...
};
```

#### Estilo
Além das opções acima, é possível configurar a aparência da central da privacidade usando _css_ diretamente. Para isso funcionar crie um tag `<style>` antes do código do central e aplique as mudanças desejadas.

A central da privacidade tem um "tema" associado. É necessário escolher um dos temas e customizá-lo. São três temas hoje: _blue_, _light_ e _dark_.

Para customizar o tema, a melhor forma é utilizar as variáveis CSS que aplicam a mudança de forma global. As variáveis disponíveis são as seguintes:

 * _--main-color_: cor principal do texto
 * _--bg-color_: cor de fundo
 * _--dark-bg-color_: cor de fundo escura
 * _--highlight-color_: cor de destaque
 * _--btn-border_: borda dos botões
 * _--btn-color_: cor do texto dos botões
 * _--btn-bg_: cor de fundo dos botões

**IMPORTANTE**
```
    Nunca utilize a classe .svelte-xyz como seletor para customizar o estilo dos elementos, uma vez que esta classe é renomeada a cada nova versão da central da privacidade
```

##### Exemplos:

_Para alterar a cor dos botões_ (tema _blue_)
```html
<style>
    .dgbn.blue {
        --btn-bg: blue !important;
        --btn-color: red !important;
    }
</style>
<script>/* código gerado pela Dogma para o seu site */</script>
```

_Para alterar a cor de fundo do central_ (tema _light_)  
```html
<style>
    .dgbn.light .footer {
        background-color: yellow !important;
    }
</style>
<script>/* código gerado pela Dogma para o seu site */</script>
```

_Para alterar o tamanho da fonte_ (tema _light_)  
```html
<style>
    .dgbn.light p {
        font-size: 2em !important;
    }
</style>
<script>/* código gerado pela Dogma para o seu site */</script>
```

__Para garantir que css será efetivo é importante usar o nome do tema no seletor css, como no exemplo acima__
