# Pedidos de Titulares
A Dogma oferece a possibilidade de fazer a gestão dos pedidos de acesso, remoção ou retificação de dados. O _workflow_, ou fluxo, para o atendimento do pedido é o mesmo, não importa o tipo do pedido.

## Tipos de Pedido
São 3 os tipos de pedidos de titulares que a Dogma aceita:
 1. Pedidos de Acesso (_Subject Access Request_ ou SAR)
 2. Pedidos de Remoção (_Data Deletion Request_ ou DDR)
 3. Pedidos de Retificação (ou RET)

### Pedidos de Acesso
Nos pedidos de acesso, o objetivo é retornar ao titular um relatório indicando quais atividades de tratamemento são realizadas para ele, qual a base legal que justifica cada atividade de tratamento e quais são os dados, do titular em particular, que estão envolvidos em cada atividade. É neste momento que a Dogma precisa buscar ou encontrar os dados do titular em particular.

A forma como a Dogma busca os dados variam de lugar para lugar, dependendo das configurações indicadas na opção "Integração" de cada lugar.

Para atender um pedido de acesso a Dogma parte do perfil do titular indicado no momento da criação do pedido. Com base no perfil do titular, a Dogma encontra quais atividades de tratamento estão relacionadas com o perfil e busca os dados nos lugares com dados relacionados às atividades utilizando as chaves de busca informadas na transição do estágio 'Novo' para 'Identificados' no _workflow_ de atentimento de pedidos.

Apenas os dados relacionados com as atividades de tratamento e com o escopo SAR farão parte integrante do relatório final.

### Pedidos de Remoção
Nos pedidos de acesso, o objetivo é retornar ao titular um relatório indicando quais dados foram removidos e quais dados foram mantidos, justificando o motivo da remoção ou manutenção do dado em função da atividade de tratamento e base legal relacionadas a cada dado.

O critério para a remoção ou manutenção de um dado é simples: um dado deve mantido caso exista pelo menos uma atividade de tratamento cuja base legal exija a manutenção do dado, caso contrário ele pode/deve ser excluído.

Da mesma forma como nos pedidos de acesso, a forma como a Dogma remove o dado vai depender das configurações indicadas na opção "Integração" de cada lugar.

Apenas dados com o escopo DDR serão removidos.

### Pedidos de Retificação
*TODO*

## Estágios do Workflow
Para todos os pedidos o _workflow_ de execução é o mesmo. As fases ou estágios de execução são as seguintes:

 1. Pedidos Novos
 2. Pedidos Identificados
 3. Pedidos Negados
 4. Pedidos Executando
 5. Pedidos Aguardando
 6. Pedidos Prontos
 7. Pedidos Disponíveis

### Pedidos Novos
Quando um pedido é criado por um titular ele entra na fase 'Novos'. Para ser aceito pela Dogma, o titular precisa preenher o formulário de acordo com o perfil selecionado e passar pelo processo de verificação do email usando um _OTP_ (_One Time Password_). A Dogma não verifica a veracidade dos dados informados pelo titular, apenas se o email indicado está sob o controle do titular no momento da criação do pedido.

### Pedidos Identificados
Quando é possível verificar a veracidade dos dados fornecidos pelo titular na fase anterior pode-se aceitar o pedido. Neste momento é necessário informar as chaves de busca de todos os lugares que possuem dados do titular. Isso é feito para que a localização dos dados na fase 'Executando' seja simplificada.

### Pedidos Negados
Quando não é possível verificar a veracidade dos dados fornecidos pelo titular na fase anterior pode-se negar o pedido. Neste caso o titular é informado com o motivo da negativa e pode fazer outro pedido, se desejar, retificando os dados informados.

### Pedidos Executando
O pedido não começa a ser executado imediatamente após a identificação do titular. É necessário que o pedido esteja na fase 'Executando' para que a Dogma efetivamente execute o pedido. A execução do pedido depende da localização do dado com base nas chaves de busca e do tipo do pedido que foi feito pelo titular:

#### Pedidos de Acesso    
Para pedidos de acesso, a Dogma busca os dados do titular em todos os lugares relacionados às atividades de tratamento do perfil do titular.

#### Pedidos de Remoção  
Para pedidos de remoção, a Dogma faz a remoção dos dados nos lugares necessários.

#### Pedidos de Retificação  
Para pedidos de retificação, a Dogma faz a retificação dos dados necessários.

A forma como a Dogma busca, remove ou retifica os dados depende da configuração feito na opção 'Integração' cada lugar. 

### Pedidos Aguardando
Os pedidos na fase 'Aguardando' estão parados.

### Pedidos Prontos
Os pedidos na fase 'Prontos' já executaram e geraram o relatório final, mas ainda não estão disponíveis para o titular que criou o pedido. Para que o titular possa fazer o download do relatório é necessário que este esteja na fase 'Disponíveis'.

### Pedidos Disponíveis
Os pedidos na fase 'Disponíveis' estão disponíveis para download por parte do titular. O download pode ser feito pela Central da Privacidade.

## Transições de Estado
É possível automatizar completamente as transições entre as fases ou estágios no _workflow_ de pedidos, desde que a Dogma esteja configurada corretamente para aplicar as transições. Além disso, é necessário implementar um _endpoint http_ que irá indicar para a Dogma, pedido a pedido, se a transição deve ocorrer e, em alguns casos, os dados necessários para a próxima fase.

Existem 3 transições automáticas possíveis:  
 1. Transição de 'Novos' para 'Identificados' ou 'Negados'
 2. Transição de 'Identificados' para 'Executando'
 3. Transição de 'Prontos' para 'Disponíveis'

### Configuração e Enpoints
A possibilidade transição automática de cada fase deve ser configurada no painel de controle da Dogma. Devem ser indicadas as seguintes informações:

 1. A _url_ do endpoint que será chamado, via _POST_, para decidir se a transição deve ocorrer ou não
 2. (Opcional) O nome do _header_ e o _token_ (valor do header) de autenticação que serão usados na chamada _http_ da _url_

O _body_ das chamadas _http_ que a Dogma fará na _url_ indicada é o seguinte:

```json
{
    "id"      : "",
    "type"    : "",
    "status"  : "",
    "profile" : "",
    "keys"    : {},
}
```

Nas transição da fase 'Novos' para 'Identificados' ou 'Negados', além dos dados acima, a Dogma informa os lugares e os nomes dos campos que fazem parte da chave de busca de cada lugar. Isto é necessário para que o _endpoint_ possa informar os valores de cada lugar/campo respectivamente. O formato é o seguinte:

```json
{
    "places": [
        { "name": "", "fields": [{"name":"", "required": true}] },
        { "name": "", "fields": [{"name":"", "required": true}] },
    ]
}
```

Campo     | Formato (javascript) | Descrição
----------|----------------------|------------
_id_      | String               | Número do protocolo do pedido 
_type_    | String               | Tipo do pedido (SAR, DDR ou RET)
_status_  | String               | Nome da fase ou estágio atual do pedido
_profile_ | String               | Nome do perfil do titular
_keys_    | Object               | Objeto javascript (Map) com dados informados pelo titular ao criar o pedido
_places_  | Array                | Array de objetos representando os lugares e os campos de cada lugar. 

O formato esperado da resposta do _endpoint_ é o seguinte:

```json
{
    "allowed" : true,
    "to"      : "",
    "reason"  : "",
    "data"    : {}
}
```

Campo     | Formato (javascript) | Descrição
----------|----------------------|------------
_allowed_ | Boolean              | Indica se a transição deve ocorrer de forma automática 
_reason_  | String (opcional)    | Quando necessário, indica o motivo da negação do pedido
_to_      | String (opcional)    | Quando existir mais de uma opção para a próxima fase, o campo `to` indica qual o nome da próxima fase. Veja tabela abaixo para os valores possíveis.
_data_    | Object (opcional)    | Quando necessário, indica os dados que serão informados para a Dogma na transição de fase


Os valores possíveis para o campo _to_ e _data_ variam de acordo com a fase atual e a próxima fase no _workflow_:

Estado Atual     | Valor do campo _to_  | Valor do campo _data_ | Descrição
-----------------|----------------------|-----------------------|----------- 
_CREATED_        | _DENIED_             | Objeto Javascript     | Contém a justificativa
_CREATED_        | _IDENTIFIED_         | Objeto Javascript     | Contém os valores das chaves de busca
_IDENTIFIED_     | _EXECUTING_          | Vazio                 | 
_REPORT SUCCESS_ | _REPORT AVAILABLE_   | Vazio                 | 


## Exemplos
Nos exemplos abaixo estamos usando o [httpie](https://httpie.io/) e o [_endpoint_](http://api-dev.dogma.legal:9006/transition) de testes da Dogma.

### Transição de 'Novos' para 'Identificados' ou 'Negados'

#### Pedido aceito
Requisição feita pela Dogma:
```
http http://api-dev.dogma.legal:9006/transition \
    id=NUMERO_PROTOCOLO \
    profile=NOME_PERFIL \
    type=SAR \
    status=CREATED \
    keys:='{"email":"pessoa@empresa.com"}' \
    places:='[{"name":"Lugar X", "fields":[{"name":"cpf", "required":true}]}]'
```

Resposta:
```json
{
    "allowed": true,
    "data": [
        {
            "field": "cpf",
            "place": "Lugar X",
            "value": "679.262.600-70"
        }
    ],
    "to": "IDENTIFIED"
}
```

#### Pedido negado
Resposta:
```json
{
    "allowed": false,
    "reason": "Não foi possível identificar o titular com base nos dados informados",
    "to": "DENIED"
}
```

### Transição de 'Identificados' para 'Executando'
Requisição feita pela Dogma:
```
http http://api-dev.dogma.legal:9006/transition \
    id=NUMERO_PROTOCOLO \
    profile=NOME_PERFIL \
    type=SAR \
    status=IDENTIFIED \
    keys:='{"email":"pessoa@empresa.com"}' \
```

Resposta:
```json
{
    "allowed": true,
    "to": "EXECUTING"
}
```

### Transição de 'Prontos' para 'Disponíveis'
Requisição feita pela Dogma:
```
http http://api-dev.dogma.legal:9006/transition \
    id=NUMERO_PROTOCOLO \
    profile=NOME_PERFIL \
    type=SAR \
    status:="REPORT SUCCESS" \
    keys:='{"email":"pessoa@empresa.com"}' \
```

Resposta:
```json
{
    "allowed": true,
    "to": "REPORT AVAILABLE"
}
```

