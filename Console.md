# Painel de Controle
O [Painel de Controle](https://console.dogma.legal) oferece uma série de funcionalidades para os gestores e encarregados.

 * Gestão de Cookies
 * Gestão de Políticas de Privacidade
 * Gestão de Consentimentos
 * Gestão dos Pedidos dos Titulares (acesso, remoção e retificação)
 * Gestão do Inventário de Dados
 * Gestão de Operadores
 * Relatórios
