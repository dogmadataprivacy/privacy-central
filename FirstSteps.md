
# Primeiros Passos

## Crie uma conta na Dogma
Dentro do site, no canto superior direito da tela, você irá encontrar o botão "Cadastro", assim como na imagem abaixo. Ao clicar, basta seguir as instruções na tela para finalizar seu registro. 

![tela do site](img/first-steps/telasite.png "Home Dogma")

![tela inicial](img/first-steps/cadastro.png "Criar conta")

![tela de cadastro](img/first-steps/infocadastro.png "Tela de Cadastro")

## Cadastrando Sua Empresa
O primeiro passo para usar a Dogma é cadastrar sua empresa. Você pode cadastrar quantas empresas quiser, 
mas lembre-se que as configurações e outras funcionalidades são separadas por empresas e sites.

![cadastrar empresa](img/first-steps/adicionarempresa.png "Adicione sua empresa")

![informações da empresa](img/first-steps/cnpj.png "Informações da empresa")


## Cadastrando Seu Site
Após ter feito o cadastro da sua empresa, adicione também o site em que deseja ter a Central da Privacidade.  

![cadastrar site](img/first-steps/cadastrosite.png "Adicione seu site")

Você pode registrar quantos sites precisar mas a licença é única para cada um deles, ou seja, para cada site registrado, precisará pagar uma licença diferente.

![info domínio](img/first-steps/dominio.png "Informe o domínio")

Ao adicionar um novo site, você precisará preencher o campo "Domínio". O domínio do site é o nome registrado no [registro.br](http://registro.br). Por exemplo, para o site http://www.globo.com, o domínio é **globo.com**. Os outros componentes da _url_ como protocolo `http://` e _host_ `www` não fazem parte do nome do domínio.

Em caso de dúvidas fale com o nosso [suporte](mailto:suporte@dogma.legal).



## Pronto!
Com isso feito, agora você pode ir para o próximo passo e integrar a nossa [Central da Privacidade](./PrivacyCentral.md) com o seu site.
