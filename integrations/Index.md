# Integração com sistemas externos
Após receber e validar um pedido de titular, do Dogma Privacy Manager tentará acessar ou remover os dados para cada [Lugar](/DataMapping.md) cadastrado no inventário. A forma com que os dados são acessados ou extraídos varia de Lugar para Lugar, conforme o modelo de integração definido individualmente para cada Lugar.

**Lugares**
```  
    Na Dogma um Lugar é onde o tratamento de dados ocorre. Normalmente onde os dados estão armazenados. Lugares podem ser sistemas/softwares ou lugares físicos

```

Existem 5 formas de integração disponíveis: manual, api, acesso ao banco de dados, robô e plugin.

## Integração Manual
Este é o modelo de integração no qual não existe integração. Ou seja, o acesso ou remoção de dados deve ser feita de forma manual.  
Veja mais detalhes [aqui]().

## Integração via API
Esté é o modelo de integração no qual o Dogma Privacy Manager interage diretamente com os sistemas da sua empresa via API para fazer o acesso ou remoção de dados.  
Veja mais detalhes [aqui](./Api.md).

## Integração via Robô
Esté é o modelo de integração no qual o Dogma Privacy Manager usa robôs de automação para fazer o acesso ou remoção de dados.  
Veja mais detalhes [aqui]().

## Integração via Plugin
Esté é o modelo de integração no qual o Dogma Privacy Manager usa plugins desenvolvidos por sistemas de mercado para fazer o acesso ou remoção de dados.  
Veja mais detalhes [aqui]().

## Integração via Banco de Dados
Esté é o modelo de integração no qual o Dogma Privacy Manager acessa diretamente o banco de dados da sua empresa para fazer o acesso ou remoção de dados.  
Veja mais detalhes [aqui]().
