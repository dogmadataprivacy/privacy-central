# API
Esté é o modelo de integração no qual o Dogma Privacy Manager interage diretamente com os sistemas da sua empresa via API para fazer o acesso ou remoção de dados.  

## Pedidos de Acesso
Para conseguir criar um relatório para o titular, indicando quais dados são usados pela organização, é necessário que o Dogma Privacy Manager tenha acesso às informações do titular.

**Importante**
```
O Dogma Privacy Manager não cria um "banco de dados paralelo" dos dados da sua organização. 
O acesso é feito somente durante a execução do pedido do titular, nunca antes. 
Após conclusão do pedido as informações são descartadas.

```

A acesso é feito fazendo uma chamada _http_ do tipo _POST_ no _endpoint_ indicado na configuração.

O _body_ do _request_ tem as seguintes informações:  
1. **request**: dados do pedido, tais como id, tipo, etc.  
2. **keys**: chaves de busca para localizar os dados do titular.  
3. **fields**: campos.  

Exemplo:

```json
{
    "request": {
      "id": "zzz", 
      "type": "SAR", 
      "place": "system_id"
    },
    "keys": [
      {"name": "id", "value": "1"}
    ],
    "fields": [
        {"name": "name"		, "required": true},
        {"name": "last_name", "required": true},
        {"name": "phone"	, "required": false}
    ]
}
```

#### Request
O atributo _request_ tem os dados gerais do pedido do titular:
 1. **id**: o id do pedido (gerado pela Dogma).  
 2. **type**: o tipo do pedido: **SAR** para pedidos de acesso, **DDR** para pedidos de remoção e **RET** para pedidos de retificação.  
 3. **place**: o id do sistema ou lugar
#### Keys
O atributo _keys_ fornece as chaves de busca para localizar os dados do titular.
#### Fiels
Os campos que devem ser devolvidos (SAR), removidos (DDR), ou retificados (RET)

### Headers e Autenticação
É possível indicar um _header_ de autenticação na chamada _http_ na configuração. O _header_ e o valor serão usados na chamada _http_ e o serviço que recebe a chamada deve validar o header antes de responder.

Além do _header_ de autenticação é necessário passar o _header_ "_content-type_" com o valor "_application/json_".

### Respostas

#### Sucesso
As respostas devem ser no formato _json_. Os dados retornados devem seguir a indicação _required_ dos campos no atributo _fields_ do _request_. Ou seja, a resposta deve conter todos os campos "_required: true_" do atributo _fields_.  

Exemplo:  
**Http Status Code**: 200
```json
{ 
    "response": {
        "name": "José",
        "last_name": "Milagres",
        "phone": "(11) 455 877 993"
    }
}
```

## Sandbox
A Dogma fornece um _endpoint_ para testes da api. Segue o exemplo completo abaixo:  

Request
```bash
curl -X POST -H 'Content-Type: application/json' \
-d '{"request":{"id": "1234", "type": "SAR", "source": "sys1"}, "keys":[{"name": "id", "value": "1"}], "fields": [{"name": "name", "required": true}]}' \
'http://api-dev.dogma.legal:9006/lgpd'
```

Response
```bash
HTTP/1.1 200 OK
Vary: Origin
Content-Type: application/json

{
    "response": {
        "name":"José"
    }
}
```

