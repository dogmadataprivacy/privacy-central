# Cookies

Você primeiro precisa criar as categorias de cookies.

![Adicionar categoria](img/cookie-panel/addcategory.png "Adicionar Grupo/Categoria")


![Definições da categoria](img/cookie-panel/cookiecategory.png "Informações da categoria")


Em seguida, pode adicionar cookies manualmente dentro das categorias 


![Adicionando cookie manualmente](img/cookie-panel/manualcookie.png "Adicionando manualmente o cookie")

ou pode importar uma lista com todos os cookies de uma só vez.


![Botão lista de cookies](img/cookie-panel/listbutton.png "Botão para importar lista")


  Atualmente, existem extensões para Chrome e Mozilla que conseguem criar arquivos com todos os cookies do seu site. Um deles é o [CookieManager - Cookie Editor](https://chrome.google.com/webstore/detail/cookiemanager-cookie-edit/hdhngoamekjhmnpenphenpaiindoinpo). 

Ao clicar no botão Importar Cookies, selecione o arquivo referente a lista de cookies do seu site e confirme.
 
 Com a lista aberta na Dogma, basta dizer para qual categoria cada cookie deve ir e os demais detalhes sobre ele( definição, duração, propósito).


 ![Lista de cookies aberta](img/cookie-panel/importcookies.png "Importando lista de cookies")


