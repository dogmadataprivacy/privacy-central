# Central da Privacidade

A Central da Privacidade oferece acesso, por parte dos titulares de dados, às principais funcionalidades da Dogma:

1. Gestão de Cookies
2. Gestão de Consentimentos
3. Pedidos (acesso, remoção e retificação de dados)

![central da privacidade](img/privacy-central/banner.png "Central da Privacidade")

É pela Central da Privacidade que seus clientes e usuários farão as configurações e requerimentos relacionados com a LGPD.

## Gestão de Cookies
A Central da Privacidade pertime indicar quais cookies seu site usa e oferece aos titulares uma maneira simples e centralizada de aceitar, ou recusar, a utilização dos cookies de forma individualizada ou em grupo.

![cookies](img/privacy-central/cookies-1.png "Cookies")


![cookies](img/privacy-central/cookies-2.png "Cookies")


![cookies](img/privacy-central/cookies-3.png "Cookies")

Quando um titular do seu site aceitar ou recusar o uso de um cookie e/ou grupo, a Dogma grava essa escolha e disponibiliza isso de duas formas principais:

1. Consulta no [Painel de Controle](https://console.dogma.legal)
2. Pela [API Javascript](./JavascriptApi.md)

IMPORTANTE
```
    A Dogma faz apenas o registro do aceite da utilização do cookie. O descarte efetivo do cookie, caso seja necessário, é de responsabilidade do seu site. 
    Isso ocorre pois alguns cookies não podem ser acessados via javascript.
```

Veja como ser notificado das alterações na configuração via javascript [aqui](./JavascriptApi.md).

### Identificação dos Titulares
Como a Dogma sabe quem é o titular que está usando a Central da Privacidade?  
Para saber quem é o titular a Dogma faz uso de 2 cookies: o primeiro cookie é criado e gerenciado pela Dogma e o segundo, se existir, é criado e gerenciado pelo seu site.

Para saber mais sobre o uso dos cookies para identificar os titulares veja a página [Cookies](./Cookies.md)

## Gestão de Consentimentos
A Central da Privacidade oferece uma forma simples e centralizada de coletar os consentimentos para suas atividades de tratamento.  
Para cada atividade de tratamento que usa o consentimento como base legal, um pedido de consentimento é oferecido ao usuário, informando a finalidade do tratamento, quais dados são utilizados, com quem os dados são compartilhados e por quanto tempo.

![cookies](img/privacy-central/consent.png "Cookies")

As escolhas dos titulares (aceite ou revogação) ficam gravadas na Dogma com a identificação do titular, a atividade, data e hora e estão disponíveis no [Painel de Controle](https://console.dogma.legal) ou pela nossa API de integração.

IMPORTANTE
```
A Dogma utiliza os mesmos cookies descritos acima para identificar os titulares
```

## Gestão de Pedidos
A Central da Privacidade oferece um canal de comunicação digital entre os titulares e a sua empresa. Pela Central da Privacidade é possível
fazer pedidos (acesso, remoção e retificação) e acompanhar o andamento e o resultado de cada pedido.  

Antes de criar um pedido, os titulares são obrigados a se identificar. 
As informações que devem ser preenchidas são configuradas pelo encarregado no [Painel de Controle](https://console.dogma.legal).  

No exemplo abaixo, o encarregado indicou que os titulares devem fornecer o Nome, Email e Telefone para possibilitar a identificação do titular.  


![pedidos](img/privacy-central/inquiry-1.png "Pedidos de Titulares")

A Dogma envia um código de validação para o titular e aguarda a inserção do código antes de criar um pedido.  


![pedidos](img/privacy-central/inquiry-2.png "Pedidos de Titulares")

Ao inserir o código, o titular escolhe qual tipo de pedido gostaria de fazer.  

Todos os pedidos são identificados por um número de protocolo único.  


![pedidos](img/privacy-central/inquiry-4.png "Pedidos de Titulares")

Somente após a conclusão deste pedido é que o titular pode criar outro. 


![pedidos](img/privacy-central/inquiry-5.png "Pedidos de Titulares")

O relatório com o resultado do pedido fica disponível para download assim que o DPO liberar o acesso.  


![pedidos](img/privacy-central/inquiry-6.png "Pedidos de Titulares")

Todo o histórico do pedido fica disponível para o titular. 


![pedidos](img/privacy-central/inquiry-7.png "Pedidos de Titulares")

O relatório final reúne todas as informações pessoais do titular, agrupados por atividade, com a indicação da base legal aplicada. Veja [aqui](./report.pdf) um relatório de exemplo.  

### Próximos passos:

* [Inserção no Site e Customização](./InsertionAndCustomization.md)
* [API Javascript](./JavascriptApi.md)
