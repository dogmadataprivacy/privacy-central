# Data mapping - Inventário de dados


## Lugares
Os lugares são todos aqueles onde estão os dados pessoais. Sejam eles físicos ou eletrônicos. 


![Lugares](img/mapeamento/places.png "Adicione um lugar")

Aqui você irá adicionar portanto, pastas, gavetas, softwares que sua empresa usa que possam ter dados pessoais dentro deles. 

![Lugares](img/mapeamento/Addsistema.png "Adicionando um sistema")


E depois, pra cada um desses lugares, poderá indicar quais são os dados pessoais em cada um.

![Lugares](img/mapeamento/placeoptions.png "Opções dentro de um lugar")


![Lugares](img/mapeamento/addfield.png "Adicionando um campo ao lugar")

Ao final, terá algo parecido com isto:


![Lugares](img/mapeamento/placewfield.png "Lugar com campo")


## Atividades
As atividades são todos os tratamentos de dados realizados pela sua empresa e por isso, cada um deles deve estar fundamentado em uma base legal bem como ter uma finalidade específica indicada. Quando adicionar uma atividade,você deve selecionar a base legal que será usada e também um propósito para o tratamento. 

![Atividades](img/mapeamento/activity.png "Adicionar uma atividade")

![Atividades](img/mapeamento/addactivity.png "Adicione um lugar")


Para cada tratamento existe uma lista de dados pessoais que estão envolvidos. 


![Atividades](img/mapeamento/addedactivity.png "Atividade definida")


Ao clicar nas projeções de uma atividade, você poderá designar quais dados pessoais estão compreendidos por aquela atividade. 
Serão mostrados todos os lugares cadastrados e também todos os campos inseridos neles para que você selecione quais são usados para o tratamento.

![Atividades](img/mapeamento/addprojection.png "Faça projeções para a atividade")


![Atividades](img/mapeamento/addedprojection.png "Atividade projetada sobre um campo")