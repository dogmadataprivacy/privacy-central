
# Primeiros Passos

## Cadastrando Sua Empresa
## Cadastrando Seu Site


O objetivo da plataforma é atender os clientes em duas frentes: ajudar o
encarregado de dados a organizar e gerir os dados pessoais dentro dos
sistemas da empresa e também permitir que o titular possa, dentro do site da
empresa, configurar suas preferências de privacidade e realizar pedidos para
o encarregado de dados.

Para cada uma dessas frentes, temos uma ferramenta específica. Para seu cliente, temos a Central de privacidade, que é vista desta forma dentro do site: Se quiser ver mais sobre a Central, vá até a sessão [Central da Privacidade](##Central da Privacidade).

[Central](img src="./felipe/Downloads/Instruções/Visão do usuário do site/Central de privacidade.png")

Mas para ter acesso a Central e a ferramenta do DPO, precisamos que você primeiro se cadastre em nosso site www.dogma.legal. 

[Início do cadastro](img src="./felipe/Downloads/Instruções/Cadastro na plataforma/Iniciar cadastro na Dogma.PNG")

Clique no botão “cadastro” na parte superior direita da tela e a seguir, preencha as informações que estão na tela. 

Dentro do seu Cadastro, cadastre uma empresa e um site. 

[Adicionar empresa](img src="./felipe/Downloads/Instruções/Visão do encarregado/Escolher a empresa.PNG")

[Adicionar site](img src="./felipe/Downloads/Instruções/Visão do encarregado/Sites/Adicionar site.PNG")

[Informações do site](img src="./felipe/Downloads//Visão do encarregado/Sites/Cadastro do novo site.PNG")

Com o site cadastrado, é possível configurar a Central para que ela apareça para seu usuário.