# API Javascript
É possível interagir com a Central da Privacidade via javascript em qualquer página que tenha o código da Central da Privacidade instalado.

Todas as páginas com o código da Central da Privacidade exportam a API Javascript da dogma através do objeto 'dogma' no escopo global da janela.  

```javascript
window.dogma
```

As principais funcionalidades disponíveis são as seguintes:

1. Cookies
2. Consentimentos
3. Pedidos
4. Banner

## Cookies
```javascript
const cookies = window.dogma.cookies()
```

O objeto _cookies_ tem os seguintes métodos:  

1. _list()_ para listar os grupos e cookies existentes
2. _accept()_ para aceitar ou recusar um cookie e/ou grupo de cookies
3. _onChange()_ chamada de _callback_ quando um cookie e/ou grupo é aceito ou recusado

### Listando os Cookies existentes
Para listar os cookies cadastrados na plataforma use o método _list()_ do objeto cookies. 

```javascript 
await window.dogma.cookies().list().then(r => r.json())
```

A chamada retorna a lista de grupos e cookies em uma _Promise_. Exemplo:
```javascript
[
    {
        "id": 1,
        "name": "Nome do Grupo",
        "description": "Descrição do Grupo",
        "required": true, // indica se todos os cookies deste grupo são obrigatórios
        "preferences":[{   
            "id"        :1,
            "accepted"  :false,
            "name"      :"nome_do_cookie",
            "provider"  :"dominio.do.cookie.com.br",
            "kind"      :"http",
            "duration"  :"unlimited",
            "purpose"   :"descrição do propósito"
        }]
    }
]
```

###  Aceitando ou recusando Grupos de Cookies ou Cookies individualmente
Para aceitar ou recusar listar os cookies cadastrados na plataforma use o método accept(accepted, group, cookie)_ do objeto cookies. 

Parâmetros:  

 *  _accepted (**boolean**): indica se o grupo/cookie foi aceito ou recusado
 *  _group_  (**number**): o id do grupo
 *  _cookie_ (**number**): o id do cookie. Este parâmetro é opcional. Caso seja omitido o valor de _accepted_ será aplicado à todos os cookies do grupo

Exemplos:  

Para aceitar todos os cookies do grupo 1
```javascript 
window.dogma.cookies().accept(true, 1)
```

Para recusar apenas o cookie 99 do grupo 42
```javascript 
window.dogma.cookies().accept(false, 42, 99)
```

**IMPORTANTE**
```  
    O aceite ou recusa de um de um grupo e/ou cookie  não implica na remoção e futura não utilização 
    do cookie por parte do site, uma vez que alguns cookies não podem ser removidos via javascript.

    Leia a seção "Recebendo Notificações de aceite ou recusa de Grupos e Cookies" para mais informações.
```

### Recebendo Notificações de aceite ou recusa de Grupos e Cookies
Registre uma função de _callback_  no método _onChange(callback)_ para ser notificado quando um titular de dados aceita ou recusa a utilização de um grupo e/ou cookie.
```javascript 

funtion myHandler(evt) {
    console.log(`Evento: ${evt.event}`)
    console.log(`Sucesso: ${evt.success}`)
    console.log(`Resposta: ${evt.response}`)
    console.log(`Dados: ${evt.data}`)
    console.log(`Erro: ${evt.error}`)
}

window.dogma.cookies().onChange(myHandler)
```

Esta função deve ser utilizada pelo site para efetivamente remover os cookies que foram recusados ou criar os cookies que foram aceitos.


## Consentimentos
```javascript
const consent = window.dogma.consent()
```

O objeto _consent_ tem os seguintes métodos:  

1. _list()_ para listar as atividades de tratamento cuja base legal é o consentimento
2. _accept()_ para aceitar ou revogar uma atividade de tratamento
3. _onChange()_ chamada de _callback_ quando uma atividade é aceita ou revogada

IMPORTANTE
```
    A Dogma faz uso do cookie dogma-client-user para distinguir entre os titulares. Este cookie deve ser criado pela seu site e disponível para todo o domínio
```

### Listando os Consentimentos
Para listar as atividades de tratamento suja base legal é o consentimento use o método _list()_ do objeto consent. 

```javascript 
await window.dogma.consent().list().then(r => r.json())
```

A chamada retorna a lista atividades de tratament em uma _Promise_. Exemplo:
```javascript
[
    {
        "id"        : 1,
        "purpose"   : "Permissão para o envio de promoções da nossa rede de parceiros.",
        "title"     : "Envio de Emails Promocionais",
        "operators" : [],
        "fields"    : ["Nome","Email"],
        "accepted"  : false,
        "acceptedOn": "2021-01-01T00:00:00"
    }
]
```

###  Aceitando ou revogando o Consentimento
Para aceitar ou recusar atividades de tratamento cuja base legal é o Consentimentométodo accept(accepted, id)_ do objeto consent. 

Parâmetros:  

 *  _accepted (**boolean**): indica se o consentimento deve ser aceito ou recusado
 *  _id_  (**number**): o id da atividade de tratamento

Exemplos:  

Para aceitar a atividade de tratamento 1
```javascript 
window.dogma.consent().accept(true, 1)
```

Para recusar a atividade de tratamento 1
```javascript 
window.dogma.consent().accept(false, 1)
```

O _id_ da atividade de tratamento vem da lista retornada pelo método _list()_ 

## Pedidos
```javascript
const inquiries = window.dogma.inquiries()
```

O objeto _inquiries_ tem os seguintes métodos:  

1. _list()_


### Banner
```javascript
const banner = window.dogma.banner()
```

O objeto _banner_ tem os seguintes métodos:  

1. _isClosed()_ retorna _true_ quando o o banner está fechado e _false_ ou _indefined_ quando o banner está aberto 
2. _open()_ para abrir/mostrar o banner
3. _close()_ para fechar/ocultar o banner

```javascript
const banner = window.dogma.banner()

banner.close()
banner.isClosed() //retorna true
banner.open()
banner.isClosed() //retorna false

```


